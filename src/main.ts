import { fromEvent, BehaviorSubject } from "rxjs";

interface LetterState {
  letters: string[];
}

// Store-ish stuff
const initialState: LetterState = {
  letters: []
};

const state$ = new BehaviorSubject<LetterState>(initialState);

// Reducer-ish stuff
const clearAllLetters = () => {
  state$.next({
    ...state$.getValue(),
    letters: []
  });
};

const clearEarliestLetter = () => {
  state$.next({
    ...state$.getValue(),
    letters: [...state$.getValue().letters].slice(1)
  });
};

const setLetterOnEnd = (letter: string) => {
  state$.next({
    ...state$.getValue(),
    letters: [...state$.getValue().letters].concat(letter)
  });
};

const addARandomLetter = () => {
  setLetterOnEnd(generateRandomLetter());
};

// Action-ish stuff
const generateRandomLetter = () =>
  String.fromCharCode(
    Math.random() * ("z".charCodeAt(0) - "a".charCodeAt(0)) + "a".charCodeAt(0)
  );

const checkKey = (key: string) => {
  if (state$.getValue().letters[0] === key) {
    clearEarliestLetter();
  }
};

const updateLettersUI = () => {
  const letterSpans = state$
    .getValue()
    .letters.map(letter => `<span class="letter">${letter}</span>`);
  letterElement.innerHTML = letterSpans.join(" ");
};

// UI-ish stuff
const pushButton = document.getElementById("push") as HTMLButtonElement;
const resetButton = document.getElementById("reset") as HTMLButtonElement;
const letterElement = document.getElementById("letters") as HTMLDivElement;

const pushButtonClick$ = fromEvent(pushButton, "click");
const resetButtonClick$ = fromEvent(resetButton, "click");

// Subscribes
pushButtonClick$.subscribe(event => addARandomLetter());
resetButtonClick$.subscribe(event => clearAllLetters());
state$.subscribe(state => updateLettersUI());

fromEvent(document, "keydown").subscribe((event: KeyboardEvent) =>
  checkKey(event.key)
);
