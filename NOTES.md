# Learning RxJS

I've dabbled before, but we're gonna treat this doc like it's all new.

## Observables

### `fromEvent` Observable

Inspired by a [CSS Tricks article](https://css-tricks.com/animated-intro-rxjs/).

```
const button = document.getElementById("the-button"); // <button id="the-button">Button</button>

const buttonClick$ = fromEvent(button, "click");

buttonClick$.subscribe(event => {
  console.log("button pushed!");
});
```

```
const button = document.getElementById("the-button"); // <button id="the-button">Button</button>

button.addEventListener("click", () => {
  console.log("button pushed!");
});
```

**Lessons**

1. A typical convention is to use a dollar sign to indicate that a variable is an observable.
2. This is pretty similar to JavaScript's `addEventListener`, so it's a good foundation for building the mental model. When you add an event listener, you're "observing" an element, and when it emits the event that you want to do something with, it fires a function that you make. So it is with `fromEvent`: `buttonClick$` observes the element, and then we have a separate thing that subscribes to the observable. When the observable sees the event get emitted, it broadcasts that out to the subscription, which triggers the function that we make.